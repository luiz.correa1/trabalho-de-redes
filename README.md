RFC

1. !start - começa a contagem, se nenhum número foi definido por comandos, o padrão será 15 segundos.
2. !ok - para a contagem. Parar antes ou depois da quantidade de segundos definida implica em perder, enquanto parar na quantidade certa de segundos implica em ganhar. Ganhar aumenta os valores padrões das próximas contagens em 10 segundos. Quanto maior a quantidade de segundos, maior a dificuldade
3. !number - gera um número aleatório entre 10 e 20, 10 incluso. A faixa de valores pode ser alterada pelos comandos !changeBegin e !changeRange 
4. !chooseNumber X - define a contagem para o número X
5. !changeBegin X - altera a geração de número aleatório, mudando qual vai ser o menor valor válido que o comando !number gera, sem alterar o tamanho da faixa de valores
6. !changeRange X - altera o tamanho da faixa de valores. Por exemplo, se você usar !changeRange 2 depois de usar !changeBegin 0, os valores possíveis serão apenas dois: 0 e 1.
7. !changeKey X - serve para mexer na aleatoriedade do número gerado, números primos são bons para evitar repetição e favorecer a aleatoriedade do comando !number
8. !level 1 - contagem padrão: 15 segundos, intervalo: [10,20[
9. !level 2 - contagem padrão: 25 segundos, intervalo: [20,30[
10. !level 3 - contagem padrão: 35 segundos, intervalo: [30, 40[
11. !static - não altera os parâmetros depois de uma vitória
12. !double - altera os parâmetros depois de uma vitória
13. !placar - exibe o placar
14. !changeName X - muda nome a ser exibido no placar para X
15. !reset - reseta o placar
16. !see - mostra os parametros básicos do jogo
17. !help - mostra o rfc
18. !clear - limpa o terminal
19. !exit - fecha terminal
20. !load - carrega as informações do placar
21. !save - salva as informações do placar
