package trataCliente;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

import servidor.Servidor;

public class TrataCliente implements Runnable{
	private InputStream cliente;
	private Servidor servidor;
	private PrintStream saida;
	long start = 0;
	int chave = 5;
	int tamanhoIntervalo = 10;
	int inicioIntervalo = 10;
	int flagStart = 0;
	long randomNumber = 15;
	int flagStatic = 0;
	int derrotas = 0;
	int vitorias = 0;
	int loadDerrotas = 0;
	int loadVitorias = 0;
	String loadName = "";
	String name = "Player";

	
	public TrataCliente(InputStream cliente, PrintStream saida, Servidor servidor) {
		this.cliente = cliente;
		this.servidor = servidor;
		this.saida = saida;
	}
	
	public void run() {
		Scanner s = new Scanner(this.cliente);
		String resposta = null;
		while (s.hasNextLine()) {
			String code = s.nextLine();
			if (code.intern() == "!number") {
				double doubleRandomNumber = Math.random() * chave;
		        randomNumber = ((int)doubleRandomNumber)%tamanhoIntervalo + inicioIntervalo;
		        resposta = "Número de segundos : "+ randomNumber + "\nDigite !start quando estiver pronto para começar e !ok quando achar que a contagem terminou!";
			} else if (code.intern() == "!start") {
				resposta = "\nA contagem já começou!";
				start = System.currentTimeMillis();
				flagStart = 1;
			} else if (code.intern() == "!ok") {
				if (flagStart == 1) {
					flagStart = 0;
					long elapsed = (System.currentTimeMillis() - start)/1000;
					resposta = "\nTempo decorrido: " + elapsed + " segundos, Tempo necessario: " + randomNumber + " segundos";
					if (elapsed>randomNumber) {
						resposta = "\n" + (elapsed-randomNumber) + " segundos mais cedo e você teria ganhado o jogo!";
						derrotas += 1;
					} else if (elapsed<randomNumber) {
						resposta = "\n" + (randomNumber-elapsed) + " segundos a mais e você teria ganhado o jogo!";
						derrotas += 1;
					} else {
						vitorias +=1;
						resposta = "\nVOCÊ GANHOU O JOGO!!!";
						if (flagStatic == 0) {
							randomNumber += 10;
							inicioIntervalo += 10;
						}
					}
				} else {
					resposta = "\nUse o comando !start primeiro! A contagem vai iniciar pelo padrão de 15 segundos se você não tiver escolhido um número pelo comando !number ou !chooseNumber";
				}
			} else if (code.intern() == "!static"){
				flagStatic = 1;
			}else if (code.intern() == "!double"){
				flagStatic = 0;
			} else if(code.intern() == "!placar") {
				resposta = "\nPLACAR\njogador "+name+"\nvitorias: "+vitorias+"\nderrotas: "+derrotas;
			} else if(code.intern() == "!reset"){
				vitorias = 0;
				derrotas = 0;
			} else if (code.intern() == "!see") {
				resposta = "\nNumero Padrão Para Contagem: "+randomNumber+"\nIntervalo: ["+inicioIntervalo+", "+(inicioIntervalo+tamanhoIntervalo)+"]";
			} else if(code.intern() == "!help") {
				resposta = "!start - começa a contagem, se nenhum número foi definido por comandos, o padrão será 15 segundos.\r\n"
						+ "!ok - para a contagem. Parar antes ou depois da quantidade de segundos definida implica em perder, enquanto parar na quantidade certa de segundos implica em ganhar. Ganhar aumenta os valores padrões das próximas contagens em 10 segundos. Quanto maior a quantidade de segundos, maior a dificuldade\r\n"
						+ "!number - gera um número aleatório entre 10 e 20, 10 incluso. A faixa de valores pode ser alterada pelos comandos !changeBegin e !changeRange \r\n"
						+ "!chooseNumber X - define a contagem para o número X\r\n"
						+ "!changeBegin X - altera a geração de número aleatório, mudando qual vai ser o menor valor válido que o comando !number gera, sem alterar o tamanho da faixa de valores\r\n"
						+ "!changeRange X - altera o tamanho da faixa de valores. Por exemplo, se você usar !changeRange 2 depois de usar !changeBegin 0, os valores possíveis serão apenas dois: 0 e 1.\r\n"
						+ "!changeKey X - serve para mexer na aleatoriedade do número gerado, números primos são bons para evitar repetição e favorecer a aleatoriedade do comando !number\r\n"
						+ "!level 1 - contagem padrão: 15 segundos, intervalo: [10,20[\r\n"
						+ "!level 2 - contagem padrão: 25 segundos, intervalo: [20,30[\r\n"
						+ "!level 3 - contagem padrão: 35 segundos, intervalo: [30, 40[\r\n"
						+ "!static - não altera os parâmetros depois de uma vitória\r\n"
						+ "!double - altera os parâmetros depois de uma vitória\r\n"
						+ "!placar - exibe o placar\r\n"
						+ "!changeName X - muda nome a ser exibido no placar para X\r\n"
						+ "!reset - reseta o placar\r\n"
						+ "!see - mostra os parametros básicos do jogo\r\n"
						+ "!help - mostra o rfc\r\n"
						+ "!clear - limpa o terminal\r\n"
						+ "!exit - fechar terminal\r\n"
						+ "!load - guarda informações do placar\r\n"
						+ "!save - carrega informações previamente salvas do placar\r\n";
			} else if(code.intern()=="!clear") {
			    try
			    {
			        final String os = System.getProperty("os.name");

			        if (os.contains("Windows"))
			        {
			            Runtime.getRuntime().exec("cls");
			        }
			        else
			        {
			            Runtime.getRuntime().exec("clear");
			        }
			    }
			    catch (final Exception e)
			    {
			        resposta = "\nNão foi possível limpar o terminal...";
			    }
			}else if(code.intern()=="!exit") {
				System.exit(0);
			}else if (code.intern()=="!load"){
				vitorias = loadVitorias;
				derrotas = loadDerrotas;
				name = loadName;
			}else if (code.intern()=="!save"){
				loadVitorias = vitorias;
				loadDerrotas = derrotas;
				loadName = name;
			}else if(code.intern() == "!level 1") {
				tamanhoIntervalo = 10;
				inicioIntervalo = 10;
				randomNumber = 15;
				resposta = "\nLevel 1 selecionado!";
			} else if(code.intern() == "!level 2") {
				tamanhoIntervalo = 10;
				inicioIntervalo = 20;
				randomNumber = 25;
				resposta = "\nLevel 2 selecionado!";
			} else if(code.intern() == "!level 3") {
				tamanhoIntervalo = 10;
				inicioIntervalo = 30;
				randomNumber = 35;
				resposta = "\nLevel 3 selecionado!";
			} else if ("!changeKey"==code.intern().substring(0, 10).intern()) {
				try {
					chave = Integer.parseInt(code.intern().substring(11));
					resposta = "\nNúmero escolhido: " + chave;
					if(chave<0) {
						chave = -chave;
					} else if (chave==0) {
						chave += 1;
					}
				}
				catch(Exception e) {
					resposta = "\nERRO, comando mal formulado! Exemplo de uso do comando: !chooseKey 17";
				}
			} else if ("!changeName"==code.intern().substring(0, 11).intern()) {
				String aux = code.substring(11);
				name = aux;
				resposta = "Nome alterado!";
			}else if ("!chooseNumber" == code.intern().substring(0, 13).intern()) {
				try {
						randomNumber = Integer.parseInt(code.intern().substring(14));
						resposta = "\nNúmero escolhido: " + randomNumber;
					}
					catch(Exception e) {
						resposta = "\nERRO, comando mal formulado! Exemplo de uso do comando: !chooseNumber 17";
					}
			} else if ("!changeBegin"==code.intern().substring(0, 12).intern()) {
				try {
					inicioIntervalo = Integer.parseInt(code.intern().substring(13));
					resposta = "\nNúmero escolhido: " + inicioIntervalo;
				}
				catch(Exception e) {
					resposta = "\nERRO, comando mal formulado! Exemplo de uso do comando: !chooseBegin 17";
				}
			} else if ("!changeRange"==code.intern().substring(0, 12).intern()) {
				try {
					tamanhoIntervalo = Integer.parseInt(code.intern().substring(13));
					resposta = "\nNúmero escolhido: " + tamanhoIntervalo;
				}
				catch(Exception e) {
					resposta = "\nERRO, comando mal formulado! Exemplo de uso do comando: !chooseRange 17";
				}
			}
			if(resposta!=null) {
				saida.println(resposta);
				}
		 }
		s.close();
		}
	}

